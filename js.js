"use strict";

let keys = document.querySelectorAll('.keys');
let shift_right = document.querySelector('.ShiftRight');
let shift_left = document.querySelector('.ShiftLeft');
let activeKey;

function random() {
    return Math.floor(Math.random() * keys.length);
}

function activateRandomKey() {
    if (activeKey) {
        activeKey.classList.remove('active');
    }
    const randomIndex = random();
    activeKey = keys[randomIndex];
    activeKey.classList.add('active');
}

keys.forEach(elem => {
    elem.setAttribute('keyname', elem.innerText);
    elem.setAttribute('lowerCaseName', elem.innerText.toLowerCase());
    elem.setAttribute('upperCaseName', elem.innerText.toUpperCase());
});

window.addEventListener('keydown', (e) => {
    keys.forEach(elem => {
        if (e.code === elem.getAttribute('keyname') || e.key === elem.getAttribute('lowerCaseName') || e.key === elem.getAttribute('upperCaseName')) {
            elem.classList.remove('active');
            elem.classList.add('remove');
            setTimeout(() => {
                elem.classList.remove('remove');
            }, 200);
        }
    });

    if (e.code === 'ShiftLeft') {
        shift_left.classList.remove('active');
        shift_left.classList.add('remove');
        setTimeout(() => {
            shift_left.classList.remove('remove');
        }, 200);
    }

    if (e.code === 'ShiftRight') {
        shift_right.classList.remove('active');
        shift_right.classList.add('remove');
        setTimeout(() => {
            shift_right.classList.remove('remove');
        }, 200);
    }

    if (e.code === activeKey.getAttribute('keyname') || e.key === activeKey.getAttribute('lowerCaseName') || e.key === activeKey.getAttribute('upperCaseName')) {
        activateRandomKey();
    }
});

activateRandomKey();
